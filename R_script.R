###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
###
### 	R source code of manuscript "Adding insult to injury: anthropogenic noise 
### 	intensifies predation risk by an invasive freshwater fish species" 
### 	by Fernandez Declerck et al. (submitted)
###
### 	Last modified on 2022-05-19
### 	The most recent version of the code and dataset can be found on 
### 	the public repository https://gitlab.com/fxdm/fr_noise_goby
###	
### 	Project
### 	  ├── README
### 	  ├── data_functional_response.txt
### 	  ├── data_prey_behaviour.txt
### 	  └── R_script.R
###
###     R code based on R version 4.2.0 (2022-04-22)
###
###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Recommended practice: start a new R session to avoid hidden dependencies (libraries, functions)
# Or, at least, clear memory to avoid conflicts of variable names.
rm(list = ls())

###################################################
###
### Requirements and dependencies
### Load required packages and libraries
###
###################################################

# install.packages('frair')
# citation('frair')
library(frair)

# install.packages('effsize')
# citation('effsize')
library(effsize)

# install.packages('MuMIn')
# citation('MuMIn')
library(MuMIn)
options(na.action = "na.fail")

# install.packages('boot')
# citation('boot')
library(boot)

# install.packages('lme4')
# citation('lme4')
library(lme4)

# install.packages('lmtest')
# citation('lmtest')
library(lmtest)

# install.packages('glmmTMB')
# citation('glmmTMB')
library(glmmTMB)

# install.packages('fitdistrplus')
# citation('fitdistrplus')
library(fitdistrplus)

# install.packages("xlsx")
# citation('xlsx')
library("xlsx")

# install.packages('scales')
# citation('scales')
library(scales)

###################################################
###
### Dataset loading
###
###################################################

###
### Main dataset: 'data_functional_response.txt'. Dataset for the main experiment
### on the functional response of the predator. Fish behaviour was recorded 
### under two noise conditions (either boat noise or ambient noise). The 
### dataset corresponds to data frame "d" in the script.
### Variables description:
### - id: identity of the fish
### - condition: noise condition, either "boat noise" or "ambient noise"
### - prey_number: number of chironomid larvae introduced in the tank
### - prey_captured: number of chironomid larvae consumed
### - fish_mass	: fish body mass (g)
### - swim_distance: swim distance (m)
###

d <- read.table("data_functional_response.txt", header = T, dec = ".", sep = "\t")
d$id <- as.factor(d$id)
d$species <- as.factor(d$species)
d$condition <- as.factor(d$condition)
summary(d)
head(d)

###
### Secondary dataset: 'data_prey_behaviour.txt'. Dataset for the control experiment 
### on prey behaviour. Prey behaviour was recorded under two noise conditions (either 
### boat noise or ambient noise). The dataset corresponds to data frame "f" in the 
### script. We used 20 replicates with 10 replicates for ambient noise condition, 
### and 10 replicates for boat noise condition. Two focal prey larva were observed 
### per replicates. Each prey larva was observed during two time periods 
### (corresponding to two noise sequences). 
### Variables description:
### - condition: noise condition, either "boat noise" or "ambient noise"
### - replicate: number of the replicate
### - unique_id: identity of each focal larva
### - noise_sequence: number of the noise sequence (either second or third)
### - prop_inactive: proportion of time spent inactive by the focal larva
### - prop_active: proportion of time spent active by the focal larva
###

f <- read.table("data_prey_behaviour.txt", header = T, dec = ".", sep = "\t")
f$unique_id <- as.factor(f$unique_id)
f$condition <- as.factor(f$condition)
f$replicate <- as.factor(f$replicate)
f$noise_sequence <- as.factor(f$noise_sequence)
summary(f)
head(f)

###################################################
###
### Summary statistics prior analysis
###
###################################################

###
### Sample sizes for the main experiment.
###	Functional response of the predator
###

length(d$fish_mass[d$condition == "ambient"])
length(d$fish_mass[d$condition == "boat"])
table(d$condition, d$prey_number)

###
### Sample size for the control experiment 
### on prey behaviour
###

table(f$condition, f$replicate)
table(f$unique_id)
length(unique(f$unique_id))

###################################################
###
### Control analysis: no difference in 
### fish body mass between groups
###
###################################################

###
### Biological material: distribution of body mass
###

hist(d$fish_mass, xlab = "Fis body mass [g]", main = "")
mean(d$fish_mass)
sd(d$fish_mass)
min(d$fish_mass)
max(d$fish_mass)

###
### statistical tests based on multimodel inference
###

### full model
mod1 <- lm(fish_mass ~ condition*prey_number, data = d)
summary(mod1)
### multi-model inference and AICc based model selection
ms1 <- dredge(mod1, extra = "adjR^2", rank = "AICc")
ms1

### export the result table, remmber to install and load "'xlsx' package
write.xlsx(ms1, file = "mumin_mass_01.xlsx")

###
### Alternative model selection procedure
### stepwise (backward ) lLikelihood ratio tests
###

rm(mod1, mod2)
mod1 <- lm(fish_mass ~ condition*prey_number, data = d)
summary(mod1)
mod2 <- lm(fish_mass ~ condition+prey_number, data = d)
summary(mod2)
anova(mod1, mod2, test = "F")

rm(mod1, mod2)
mod1 <- lm(fish_mass ~ condition+prey_number, data = d)
summary(mod1)
mod2 <- lm(fish_mass ~ condition, data = d)
summary(mod2)
anova(mod1, mod2, test = "F")

rm(mod1, mod2)
mod1 <- lm(fish_mass ~ prey_number, data = d)
summary(mod1)
mod2 <- lm(fish_mass ~ 1, data = d)
summary(mod2)
anova(mod1, mod2, test = "F")

rm(mod1, mod2)
mod1 <- lm(fish_mass ~ condition, data = d)
summary(mod1)
mod2 <- lm(fish_mass ~ 1, data = d)
summary(mod2)
anova(mod1, mod2, test = "F")

###
### Illustration plot (not shown in the MS)
### No significant for the interaction term
### betweeen noise condition and prey number groups
###

nb <- c(4, 8, 16, 32, 64, 128, 256)
fontesizeaxis <- 1.2
fontsizelabel <- 1.5
par(mar=c(5, 5, 1,1))
plot(NULL,
		xlim = c(0.5, 7.5),
		ylim = c(0.5, 3.2),
		xaxt = "n",
		xlab = "Prey number", ylab = "Fish body mass [g]",
		cex.lab=fontsizelabel, cex.axis = fontesizeaxis)
axis(side = 1, 1:7, labels = FALSE)
mtext(nb, at = 1:7, line = 1.2, side = 1)
legend("topright", c("Ambient noise", "Boat noise"), bty = "n", pch = c(22, 22), col = c("black", "black"), pt.bg = c("white", "grey"), pt.cex = 2)
for (i in 1:7){
	x <- i-0.15
	boxplot(d$fish_mass[d$prey_number == nb[i] & d$condition == "ambient"], at = x, boxwex = 0.5, add = T, col = "white", yaxt = "n", axes = FALSE)
	points(x+rnorm(length(d$fish_mass[d$prey_number == nb[i] & d$condition == "ambient"]), 0, 0.01), d$fish_mass[d$prey_number == nb[i] & d$condition == "ambient"])
	points(x, mean(d$fish_mass[d$prey_number == nb[i] & d$condition == "ambient"]), pch = 19, col = "red")
}
for (i in 1:7){
	x <- i+0.15
	boxplot(d$fish_mass[d$prey_number == nb[i] & d$condition == "boat"], at = x, boxwex = 0.5, add = T, col = "grey", yaxt = "n", axes = FALSE)
	points(x+rnorm(length(d$fish_mass[d$prey_number == nb[i] & d$condition == "boat"]), 0, 0.01), d$fish_mass[d$prey_number == nb[i] & d$condition == "boat"])
	points(x, mean(d$fish_mass[d$prey_number == nb[i] & d$condition == "boat"]), pch = 19, col = "red")
}

###
### Illustration plot (not shown in the MS)
### No significant difference in fish body  mass between condition groups 
### (without considering prey number)
###

par(mar=c(5, 5, 1,1))
boxplot(d$fish_mass[d$condition == "ambient"], d$fish_mass[d$condition == "boat"],
			ylab = "Body mass [g]",
			xlab = "condition", xaxt= "n", col = c("white", "grey"))
mtext(c("Ambient", "Boat noise"), at = c(1,2), line = 1.2, side = 1)
# data points with slight horizontal jittering to avoid overlapping
points(1+rnorm(length(d$fish_mass[d$condition == "ambient"]), 0, 0.02), d$fish_mass[d$condition == "ambient"])
points(2+rnorm(length(d$fish_mass[d$condition == "boat"]), 0, 0.02), d$fish_mass[d$condition == "boat"])
# mean as a red dot
points(c(1,2), c(mean(d$fish_mass[d$condition == "ambient"]), mean(d$fish_mass[d$condition == "boat"])), pch = 19, col = "red")

t.test(d$fish_mass ~ d$condition)

### index of effect size (cohen's d)
cohen.d(d$fish_mass ~ d$condition)

mean(d$fish_mass)
sd(d$fish_mass)

###
### Illustration plot (not shown in the MS)
### No significant difference in fish body mass between prey number groups 
### (without considering noise condition)
###

### difference of body mass between condition prey number
par(mar=c(5, 5, 1,1))
boxplot(d$fish_mass~ d$prey_number, ,
			ylab = "Body mass [g]",
			xlab = "Prey number")
# data points with slight horizontal jittering to avoid overlapping
points(1+rnorm(length(d$fish_mass[d$prey_number == 4]), 0, 0.02), d$fish_mass[d$prey_number == 4])
points(2+rnorm(length(d$fish_mass[d$prey_number == 8]), 0, 0.02), d$fish_mass[d$prey_number == 8])
points(3+rnorm(length(d$fish_mass[d$prey_number == 16]), 0, 0.02), d$fish_mass[d$prey_number == 16])
points(4+rnorm(length(d$fish_mass[d$prey_number == 32]), 0, 0.02), d$fish_mass[d$prey_number == 32])
points(5+rnorm(length(d$fish_mass[d$prey_number == 64]), 0, 0.02), d$fish_mass[d$prey_number == 64])
points(6+rnorm(length(d$fish_mass[d$prey_number == 128]), 0, 0.02), d$fish_mass[d$prey_number == 128])
points(7+rnorm(length(d$fish_mass[d$prey_number == 256]), 0, 0.02), d$fish_mass[d$prey_number == 256])
# mean as a red dot
points(1:7, c(mean(d$fish_mass[d$prey_number == 4]),
				mean(d$fish_mass[d$prey_number == 8]),
				mean(d$fish_mass[d$prey_number == 16]),
				mean(d$fish_mass[d$prey_number == 32]),
				mean(d$fish_mass[d$prey_number == 64]),
				mean(d$fish_mass[d$prey_number == 128]),
				mean(d$fish_mass[d$prey_number == 256])), pch = 19, col = "red")

###################################################
###
### Figure 1 in the MS.
###
### This section is not a full analysis.
### Its purpose is only to draw Fig.1 in the MS.
### Extensive analyses of functional responses 
### and swim distances are provided in the following 
### sections.
###
###################################################

###
### Derivation of functional response
###

rm(fit1,fit2) # remove (previous) fits to avoid conflicts
fit1 <- frair_fit(prey_captured~prey_number, data=d[d$condition == "ambient",], response='rogersII',start=list(a = 50, h = 0.01), fixed=list(T=15/60))
summary(fit1$fit)
fit1
boot1 <- frair_boot(fit1)
boot1
fit2 <- frair_fit(prey_captured~prey_number, data=d[d$condition == "boat",], response='rogersII',start=list(a = 50, h = 0.01), fixed=list(T=15/60))
summary(fit2$fit)
fit2
boot2 <- frair_boot(fit2)
boot2

###
### Plot based on parameters estimated above
###

### size of the output window and plot parameters
windows(10,6)
layout(matrix(c(1,2),nrow=1), widths=c(2,1))
fontesizeaxis <- 1.2
fontsizelabel <- 1.5
col_ambient <- "cornflowerblue"
col_boat <- "darkorange1"
###
### Fig. 1a
###
par(mar=c(5, 5, 1,1))
plot(NULL, xlim = c(0,260), ylim = c(0,40), las = 1,
	xlab = "Initial number of prey", ylab = "Number of prey consumed",
	cex.lab=fontsizelabel, cex.axis = fontesizeaxis)
#---------------------
lines(fit1,  lwd=2, col=col_ambient,tozero=T)
drawpoly(boot1, col=alpha(col_ambient, 0.2),tozero=T,border=NA)
points(jitter(d$prey_number[d$condition == "ambient"],2),d$prey_captured[d$condition == "ambient"], pch = 21, col = col_ambient, bg = alpha(col_ambient, 0.2))
#---------------------
lines(fit2,  lwd=2, col=col_boat,tozero=T)
drawpoly(boot2, col=alpha(col_boat, 0.2),tozero=T,border=NA)
points(jitter(d$prey_number[d$condition == "boat"],2),d$prey_captured[d$condition == "boat"], pch = 21, col = col_boat, bg = alpha(col_boat, 0.2))
#---------------------
legend("topleft", c("Ambient noise", "Boat noise"), lty = 1, lwd = 2, col = c(col_ambient,col_boat), bty = "n", cex = fontesizeaxis)
mtext("(a)", side = 2, cex = 1.5, outer = FALSE, las = TRUE, at = 42, line = 3.2)
###
### Fig. 2a
###
par(mar=c(5, 5, 1,1))
plot(NULL,
		xlim = c(0.5, 2.5),
		ylim = c(0, 40),
		xaxt = "n",
		xlab = "Noise condition", ylab = "Total distance (m)",
		cex.lab=fontsizelabel, cex.axis = fontesizeaxis, las = 1)
axis(side = 1, 1:2, labels = FALSE)
mtext(c("Ambient", "Boat"), at = 1:2, line = 1.2, side = 1, cex = fontesizeaxis)
#----------------------
y <- d$swim_distance[d$condition == "ambient"]
boxplot(y, at = 1, boxwex = 1, add = T,
		border = col_ambient,
		col = alpha(col_ambient, 0.2), outline = FALSE, yaxt = "n", axes = FALSE)
points(rnorm(length(y), 1, 0.01), y, pch = 21, col = col_ambient, bg = alpha(col_ambient, 0.2))
points(1, mean(y), pch = 19, col = "black", cex= 1.2)
#----------------------
y <- d$swim_distance[d$condition == "boat"]
boxplot(y, at = 2, boxwex = 1, add = T,
		border = col_boat,
		col = alpha(col_boat, 0.2), outline = FALSE, yaxt = "n", axes = FALSE)
points(rnorm(length(y), 2, 0.01), y, pch = 21, col = col_boat, bg = alpha(col_boat, 0.2))
points(2, mean(y), pch = 19, col = "black", cex= 1.2)
#----------------------
pos <- 35
segments(1, pos, 2, pos)
segments(1, pos, 1, 30)
segments(2, pos, 2, 31)
text(1.5, pos + 2, "**", cex = 2)
mtext("(b)", side = 2, cex = 1.5, outer = FALSE, las = TRUE, at = 42, line = 3.2)

###################################################
###
### Derivation of the functional responses 
###
###################################################

###
### Shape of the functional responses: Type I, type II or type III?
### Models selection is based on AIC
###

### Condition: Ambient noise
rm(fit1_typeI,fit1_typeII, fit1_typeIII) # remove (previous) fits to avoid conflicts
### Type I
fit1_typeI <- frair_fit(prey_captured~prey_number, data=d[d$condition == "ambient",], response='typeI',start=list(a =0.5), fixed=list(T=15/60))
summary(fit1_typeI$fit)
fit1_typeI
### Type II
fit1_typeII <- frair_fit(prey_captured~prey_number, data=d[d$condition == "ambient",], response='rogersII',start=list(a = 5, h = 0.01), fixed=list(T=15/60))
summary(fit1_typeII$fit)
fit1_typeII
### Type III
fit1_typeIII <- frair_fit(prey_captured~prey_number, data=d[d$condition == "ambient",],
					response='hassIII', start=list(b=0.05, c=0.1, h=0.1), fixed=list(T=15/60))
fit1_typeIII
# Best model: Type II (smaller AIC)
AIC(fit1_typeI$fit, fit1_typeII$fit, fit1_typeIII$fit)

### Condition: Boat noise
rm(fit1_typeI,fit1_typeII) # remove (previous) fits to avoid conflicts
### Type I
fit1_typeI <- frair_fit(prey_captured~prey_number, data=d[d$condition == "boat",], response='typeI',start=list(a =0.5), fixed=list(T=15/60))
summary(fit1_typeI$fit)
fit1_typeI
### Type II
fit1_typeII <- frair_fit(prey_captured~prey_number, data=d[d$condition == "boat",], response='rogersII',start=list(a = 5, h = 0.01), fixed=list(T=15/60))
summary(fit1_typeII$fit)
fit1_typeII
### Type III
fit1_typeIII <- frair_fit(prey_captured~prey_number, data=d[d$condition == "boat",],
					response='hassIII', start=list(b=0.05, c=0.1, h=0.1), fixed=list(T=15/60))
fit1_typeIII
# Best model: Type II. Type III is less parsimonious (df = 3) and does not improve the AIC value by more than 2. Type II model should be preferred 
AIC(fit1_typeI$fit, fit1_typeII$fit, fit1_typeIII$fit)

###
### Analysis with type II functional response (RogersII).
### The prey densities was not constant during the 15-min experimental period
### (as a function of each predation event). We thus used the generalized model
### of Rogers’ random predator equation (Rogers 1972; DeLong 2021)
###

# Note about the units: the observations were 15 minutes long. The model parameter is  thus 15/60 hours,
# which mean that the unit for h is in hours^{-1}

### Comparison of the two noise conditions
### Ambient noise
rm(fit1, fit2)
fit1 <- frair_fit(prey_captured~prey_number, data=d[d$condition == "ambient",], response='rogersII',start=list(a = 5, h = 0.01), fixed=list(T=15/60))
summary(fit1$fit)
fit1
boot1 <- frair_boot(fit1)
boot1
### Boat noise
fit2 <- frair_fit(prey_captured~prey_number, data=d[d$condition == "boat",], response='rogersII',start=list(a = 5, h = 0.01), fixed=list(T=15/60))
summary(fit2$fit)
fit2
boot2 <- frair_boot(fit2)
boot2
### comparison tests (null hypothesis testing)
### --> Juliano SA (2001) Nonlinear curve fitting: Predation and functional response curves. In: Scheiner SM, Gurevitch J (eds). Design and analysis of ecological experiments. Oxford University Press, Oxford, United Kingdom. pp 178--196.)
fit1
fit2
frair_compare(fit1, fit2)

###
### Proper units for parameter a (clearance rate)
### In order to be compared with other studies (meta-analysis),
### the value of a must be expressed in termes of arena surface. 
### --> see Uiterwaal SF, Lagerstrom IT, Lyon SR, DeLong JP (2018) Data
###     paper: FoRAGE (Functional Responses from Around the Globe in all
### 	Ecosystems) database: a compilation of functional responses for
### 	consumers and parasitoids. bioRxiv:503334 http://dx.doi.org/10.1101/503334
###

arena_surface <- 25*60/10000 ## units = m²

# a: ambient condition (units = m² per predator and per hour)
# Coefficients (original data):
#     a     h     T
# 2.338 0.020 0.250
# 95% BCa confidence intervals (for more info, see ?confint.frboot):
# Coefficient  CI Type        Lower   Upper
# a            BCa            0.958   7.191
# h            BCa            0.01    0.04
fit1
boot1
2.338*arena_surface # a, units = m² per hour per predator
0.958*arena_surface # IC95%
7.191*arena_surface # IC95%
# h units = hours par prey

# a: boat condition (units = m² per predator and per hour)
# Coefficients (original data):
#     a     h     T
# 7.563 0.012 0.250
# 95% BCa confidence intervals (for more info, see ?confint.frboot):
# Coefficient  CI Type        Lower   Upper
# a            BCa            3.774   15.155
# h            BCa            0.009   0.015
fit0.02
boot2
7.563*arena_surface # a, units = m² per hour per predator
3.774*arena_surface # IC95%
15.155*arena_surface # IC95%
# h units = hours par prey

###
### Plot: functional responses
###

# windows(10,7)
fontesizeaxis <- 1.2
fontsizelabel <- 1.5
par(mar=c(5, 5, 1,1))
plot(NULL, xlim = c(0,260), ylim = c(0,40), las = 1,
	xlab = "Initial number of prey", ylab = "Number of prey consumed",
	cex.lab=fontsizelabel, cex.axis = fontesizeaxis)
#---------------------
col_ambient <- "cornflowerblue"
lines(fit1,  lwd=2, col=col_ambient,tozero=T)
drawpoly(boot1, col=alpha(col_ambient, 0.5),tozero=T,border=NA)
points(jitter(d$prey_number[d$condition == "ambient"],2),d$prey_captured[d$condition == "ambient"], col = alpha(col_ambient, 0.5), pch=20)
#---------------------
col_boat <- "darkorange1"
lines(fit2,  lwd=2, col=col_boat,tozero=T)
drawpoly(boot2, col=alpha(col_boat, 0.5),tozero=T,border=NA)
points(jitter(d$prey_number[d$condition == "boat"],2),d$prey_captured[d$condition == "boat"], col = alpha(col_boat, 0.5), pch=20)
#---------------------
legend("topleft", c("Ambient noise", "Boat noise"), lty = 1, lwd = 2, col = c(col_ambient,col_boat), bty = "n", cex = fontesizeaxis)

###
### For comparison purposes only: plot with non-parametric bootstrapped 95%CI
### (complementary analysis not shown in the MS)
###
### The aim of this section is to illustrate that the 95%IC around the
### predicted functional response calculated using 'frair' is consistent
### with bootstrap estimates.
###

nb_boot <- 1000 ### <-- nb of boot
cont <- d[d$condition == "ambient",]
nb_cont <- length(cont$id)
boat <- d[d$condition == "boat",]
nb_boat  <- length(boat$id)
a1 <- numeric(nb_boot)
a2 <- numeric(nb_boot)
h1 <- numeric(nb_boot)
h2 <- numeric(nb_boot)
### main plot windows
windows(10,7)
fontesizeaxis <- 1.2
fontsizelabel <- 1.5
col_ambient <- "cornflowerblue"
col_boat <- "darkorange1"
par(mar=c(5, 5, 1,1))
plot(NULL, xlim = c(0,260), ylim = c(0,40), las = 1,
	xlab = "Initial number of prey", ylab = "Number of captured prey",
	cex.lab=fontsizelabel, cex.axis = fontesizeaxis)
### Bootstrap loop
prey_nb <- c(4, 8, 16, 32, 64, 128, 256)
for (i in 1:nb_boot){
	## ambient noise
	g  <- cont
	g$prey_number
	g$prey_captured_boot <- NA
	for (j in 1:length(g$prey_number)) {
		g$prey_captured_boot[j] <- sample(g$prey_captured[g$prey_number == g$prey_number[j]], 1)
	}
	rm(fit1)
	fit1 <- frair_fit(prey_captured_boot~prey_number, data=g, response='rogersII',start=list(a = 1.2, h = 0.015), fixed=list(T=15/60))
	a1[i] <- fit1$coefficients[1]
	h1[i] <- fit1$coefficients[2]
	lines(fit1,  lwd=1, col=alpha(col_ambient, 0.2),tozero=T)
	## boat noise
	g  <- boat
	g$prey_number
	g$prey_captured_boot <- NA
	for (j in 1:length(g$prey_number)) {
		g$prey_captured_boot[j] <- sample(g$prey_captured[g$prey_number == g$prey_number[j]], 1)
	}
	rm(fit2)
	fit2 <- frair_fit(prey_captured_boot~prey_number, data=g, response='rogersII',start=list(a = 1.2, h = 0.015), fixed=list(T=15/60))
	a2[i] <- fit2$coefficients[1]
	h2[i] <- fit2$coefficients[2]
	lines(fit2,  lwd=1, col=alpha(col_boat, 0.2),tozero=T)
}
#--------------------- original points
points(jitter(d$prey_number[d$condition == "ambient"],2),d$prey_captured[d$condition == "ambient"], col = alpha(col_ambient, 0.5), pch=20)
points(jitter(d$prey_number[d$condition == "boat"],2),d$prey_captured[d$condition == "boat"], col = alpha(col_boat, 0.5), pch=20)
fit1 <- frair_fit(prey_captured~prey_number, data=d[d$condition == "ambient",], response='rogersII',start=list(a = 5, h = 0.01), fixed=list(T=15/60))
lines(fit1,  lwd=1, col="black",tozero=T)
fit2 <- frair_fit(prey_captured~prey_number, data=d[d$condition == "boat",], response='rogersII',start=list(a = 5, h = 0.01), fixed=list(T=15/60))
lines(fit2,  lwd=1, col="black",tozero=T)
#---------------------
legend("topleft", c("Ambient noise", "Boat noise"), lty = 1, lwd = 1, col = c(col_ambient,col_boat), bty = "n", cex = fontesizeaxis)

###
### For comparison purposes only: bootstrapped p-values.
### Null hypothesis testing for a and h based on non-parametric bootstraps
### (complementary analysis not shown in the MS)
###
### In the MS we used the p-values provided by the function 'frair_compare()' 
### from 'frair' packages. The aim of this subsection is to provide an alternative 
### assessment of the significance of the difference in a and h between noise 
### conditions. One can also have a non-parametric estimates of these p-values
### using bootstrapped NHT (null hypothesis testing). 
###       To get a bootstrapped p-value, one can estimate the proportion of case
### where the difference in parameter value (a or h) was greater that
### the observed difference under null hypothesis. To do so, noise condition 
### within each prey density are randomly reassignment within each prey density.
###

nb_boot <- 10000
a1 <- numeric(nb_boot)
a2 <- numeric(nb_boot)
h1 <- numeric(nb_boot)
h2 <- numeric(nb_boot)
h <- d
for (i in 1:nb_boot){
	#  randomly re-assignemnt of noise condition within each prey density
	h$condition2 <- NA
	h4 <- h[h$prey_number == 4,]
	h4$condition2 <- sample(h4$condition)
	h8 <- h[h$prey_number == 8,]
	h8$condition2 <- sample(h8$condition)
	h16 <- h[h$prey_number == 16,]
	h16$condition2 <- sample(h16$condition)
	h32 <- h[h$prey_number == 32,]
	h32$condition2 <- sample(h32$condition)
	h64 <- h[h$prey_number == 64,]
	h64$condition2 <- sample(h64$condition)
	h128 <- h[h$prey_number == 128,]
	h128$condition2 <- sample(h128$condition)
	h256 <- h[h$prey_number == 256,]
	h256$condition2 <- sample(h256$condition)
	h <- rbind(h4, h8, h16, h32, h64, h128, h256)
	## ambient noise
	g  <- h[h$condition2 == "ambient",]
	rm(fit1)
	fit1 <- frair_fit(prey_captured~prey_number, data=g, response='rogersII',start=list(a = 5, h = 0.01), fixed=list(T=15/60))
	a1[i] <- fit1$coefficients[1]
	h1[i] <- fit1$coefficients[2]
	## boat noise
	g  <- h[h$condition2 == "boat",]
	rm(fit2)
	fit2 <- frair_fit(prey_captured~prey_number, data=g, response='rogersII',start=list(a = 5, h = 0.01), fixed=list(T=15/60))
	a2[i] <- fit2$coefficients[1]
	h2[i] <- fit2$coefficients[2]
}
#### observed differences in a and h
fit1 <- frair_fit(prey_captured~prey_number, data=d[d$condition == "ambient",], response='rogersII',start=list(a = 5, h = 0.01), fixed=list(T=15/60))
summary(fit1$fit)
fit2 <- frair_fit(prey_captured~prey_number, data=d[d$condition == "boat",], response='rogersII',start=list(a = 5, h = 0.01), fixed=list(T=15/60))
summary(fit2$fit)
diff_a_observed <- (fit2$coefficients[1]-fit1$coefficients[1])
diff_h_observed <- (fit2$coefficients[2]-fit1$coefficients[2])
### propoportion of simulation under H0 with greater difference in a and h
diff_a_H0 <- a2 - a1
# p-value for a
sum(diff_a_H0 > diff_a_observed)/nb_boot # 0.0424 with 10000 boostrap
diff_h_H0 <- h2 - h1
# p-value for h
sum(diff_h_H0 < diff_h_observed)/nb_boot # 0.0407 with 10000 boostrap

###################################################
###
### Swim distance of the fish
###
###################################################

###
### Exploratory plots prior analysis
###

### Visual inspection of the distribution
### Note the logarithmic scale to normalize swim distance
hist(log(d$swim_distance+0.5))

### Effect of the noise condition (see below for the statistical tests)
par(mar=c(5, 5, 1,1))
boxplot((d$swim_distance + 0.5) ~ d$condition, log = "y",
		xlab = "condition",
		ylab = "Total distance")

### No effect of fish body mass (see below for the statistical tests)
### (complementary analysis not shown in the MS)
par(mar=c(5, 5, 1,1))
plot((d$swim_distance + 0.5) ~ d$fish_mass, log = "y",
		xlab = "Body mass [g]",
		ylab = "Total distance")
points((d$swim_distance[d$condition == "boat"]+1) ~ d$fish_mass[d$condition == "boat"], pch = 19)
legend("topleft", c("Ambient noise", "Boat noise"), pch = c(1,19), bty = "n")

### No effect of prey density (see below for the statistical tests)
### (complementary analysis not shown in the MS)
plot((d$swim_distance+0.5) ~ d$prey_number, log = "xy",
		xlab = "Prey number",
		ylab = "Total distance")
points((d$swim_distance[d$condition == "boat"]+1) ~ d$prey_number[d$condition == "boat"], pch = 19)
legend("topleft", c("Ambient noise", "Boat noise"), pch = c(1,19), bty = "n")

### Boxplot with both noise condition and prey number
### (complementary analysis not shown in the MS)
nb <- c(4, 8, 16, 32, 64, 128, 256)
fontesizeaxis <- 1.2
fontsizelabel <- 1.5
par(mar=c(5, 5, 1,1))
plot(NULL,
		xlim = c(0.5, 7.5),
		ylim = c(0, 32),
		xaxt = "n",
		xlab = "Prey number", ylab = "Total distance",
		cex.lab=fontsizelabel, cex.axis = fontesizeaxis)
axis(side = 1, 1:7, labels = FALSE)
mtext(nb, at = 1:7, line = 1.2, side = 1)
legend("topright", c("Ambient", "Boat"), bty = "n", pch = c(22, 22), col = c("black", "black"), pt.bg = c("white", "grey"), pt.cex = 2)
for (i in 1:7){
	x <- i-0.15
	boxplot(d$swim_distance[d$prey_number == nb[i] & d$condition == "ambient"], at = x, boxwex = 0.5, add = T, col = "white", yaxt = "n", axes = FALSE)
	points(x+rnorm(length(d$swim_distance[d$prey_number == nb[i] & d$condition == "ambient"]), 0, 0.01), d$swim_distance[d$prey_number == nb[i] & d$condition == "ambient"])
	points(x, mean(d$swim_distance[d$prey_number == nb[i] & d$condition == "ambient"]), pch = 19, col = "red")
}
for (i in 1:7){
	x <- i+0.15
	boxplot(d$swim_distance[d$prey_number == nb[i] & d$condition == "boat"], at = x, boxwex = 0.5, add = T, col = "grey", yaxt = "n", axes = FALSE)
	points(x+rnorm(length(d$swim_distance[d$prey_number == nb[i] & d$condition == "boat"]), 0, 0.01), d$swim_distance[d$prey_number == nb[i] & d$condition == "boat"])
	points(x, mean(d$swim_distance[d$prey_number == nb[i] & d$condition == "boat"]), pch = 19, col = "red")
}

###
### Statistical tests based on multimodel inference
###

### Multi-model inference and AICc based model selection
### Full model definition
mod1 <- glm(log(swim_distance + 0.1) ~ condition*fish_mass*prey_number, data = d)
summary(mod1)
### model randk and AICc table
ms1 <- dredge(mod1, extra = "adjR^2", rank = "AICc")
ms1

### export the result table, remmber to install and load "'xlsx' package
write.xlsx(ms1, file = "mumin_swim_01.xlsx")

###
### Alternative model selection procedure
### stepwise (backward ) lLikelihood ratio tests
### (complementary analysis not shown in the MS)
###

### no effect of fish_mass
mod1 <- glm(log(swim_distance + 0.5) ~ condition + fish_mass, data = d)
summary(mod1)
mod2 <- glm(log(swim_distance + 0.5) ~ condition, data = d)
summary(mod2)
anova(mod1, mod2, test = "Chisq")

### no effect of fish_mass
mod1 <- glm(log(swim_distance + 0.5) ~ condition + prey_number, data = d)
summary(mod1)
mod2 <- glm(log(swim_distance + 0.5) ~ condition, data = d)
summary(mod2)
anova(mod1, mod2, test = "Chisq")

### effect of the condition
mod1 <- glm(log(swim_distance + 0.5) ~ condition, data = d)
summary(mod1)
confint(mod1)
mod2 <- glm(log(swim_distance + 0.5) ~ 1, data = d)
summary(mod2)
anova(mod1, mod2, test = "Chisq")
lrtest(mod1, mod2)
# inspection of the QQ-plot for model assumption
plot(mod1)

###
### Effect size index (Cohen's d)
###

cohen.d(log10(d$swim_distance + 0.5) ~ d$condition)

###
### Final figure for swim distance (as in Fig.1b)
###

### fontsize
# windows(5,7)
fontesizeaxis <- 1.2
fontsizelabel <- 1.5
par(mar=c(5, 5, 1,1))
col_ambient <- "cornflowerblue"
col_boat <- "darkorange1"
plot(NULL,
		xlim = c(0.5, 2.5),
		ylim = c(0, 40),
		xaxt = "n",
		xlab = "Noise condition", ylab = "Total distance",
		cex.lab=fontsizelabel, cex.axis = fontesizeaxis)
axis(side = 1, 1:2, labels = FALSE)
mtext(c("Ambient", "Boat"), at = 1:2, line = 1.2, side = 1, cex = fontesizeaxis)
#----------------------
y <- d$swim_distance[d$condition == "ambient"]
boxplot(y, at = 1, boxwex = 1, add = T,
		border = col_ambient,
		col = alpha(col_ambient, 0.2), outline = FALSE, yaxt = "n", axes = FALSE)
points(rnorm(length(y), 1, 0.01), y, pch = 21, col = col_ambient, bg = alpha(col_ambient, 0.2))
points(1, mean(y), pch = 19, col = "black", cex= 1.5)
#----------------------
y <- d$swim_distance[d$condition == "boat"]
boxplot(y, at = 2, boxwex = 1, add = T,
		border = col_boat,
		col = alpha(col_boat, 0.2), outline = FALSE, yaxt = "n", axes = FALSE)
points(rnorm(length(y), 2, 0.01), y, pch = 21, col = col_boat, bg = alpha(col_boat, 0.2))
points(2, mean(y), pch = 19, col = "black", cex= 1.5)
#----------------------
pos <- 35
segments(1, pos, 2, pos)
segments(1, pos, 1, 30)
segments(2, pos, 2, 31)
text(1.5, pos + 2, "**", cex = 3)

###################################################
###
### Experiment #2 : Prey behaviour
###
###################################################

summary(f)
head(f)

###
### Sample size and data structure: we used 20 replicates with 10 replicates
### under ambient noise, and 10 replicates under boat noise. Two focal fish
### were observed per replicates. Each fish was observed during two time
### period (corresponding to two noise sequences).
###

table(f$condition, f$replicate)
table(f$unique_id)
length(unique(f$unique_id))

###
### Distribution of the data : the data are not normally distributed,
### but they can be modelled using a beta distribution
### (complementary analysis not shown in the MS)
###

close.screen(all = TRUE)
par(mfrow=c(2,2),
	mar=c(3,3,1,1),  # marges: c(bottom, left, top, right)
	oma=c(2,2,0,1))
col_ambient <- "cornflowerblue"
col_boat <- "darkorange1"
z <- f$prop_active[f$noise_sequence == "NS_2" & f$condition == "ambient"]
hist(z, xlim = c(0,1), xlab = "", ylab = "", main = "Ambient noise, second noise sequence", col =col_ambient, las = 1)
z <- f$prop_active[f$noise_sequence == "NS_3" & f$condition == "ambient"]
hist(z, xlim = c(0,1), xlab = "", ylab = "", main = "Ambient noise, third noise sequence", col =col_ambient, las = 1)
z <- f$prop_active[f$noise_sequence == "NS_2" & f$condition == "boat"]
hist(z, xlim = c(0,1), xlab = "", ylab = "", main = "Boat noise, second noise sequence", col =col_boat, las =1)
z <- f$prop_active[f$noise_sequence == "NS_3" & f$condition == "boat"]
hist(z, xlim = c(0,1), xlab = "", ylab = "", main = "Boise noise, third noise sequence", col =col_boat, las = 1)
mtext("Frequency", side=2, line=0, outer=TRUE, cex=1.5, font=1)
mtext("Proportion of time active", side=1, line=0, outer=TRUE, cex=1.5, font=1)

### The data were modelled using beta distribution
f$prop_active2 <- (f$prop_active*(length(f$prop_active)-1) + 0.5) /length(f$prop_active)
hist(f$prop_active2, breaks = 15)
min(f$prop_active2)
max(f$prop_active2)
rm(fit)
fit <- fitdist(f$prop_active2, "beta")
summary(fit)
gofstat(fit)
plot(fit)

###
### Plot of individual reaction norm
### (complementary analysis not shown in the MS)
###

dev.off()

fontesizeaxis <- 1.2
fontsizelabel <- 1.5
col_ambient <- "cornflowerblue"
col_boat <- "darkorange1"
par(mar=c(5, 5, 1,1))
plot(NULL,
		xlim = c(0.9, 2.3),
		ylim = c(-0.02, 1.05),
		xaxt = "n", las = 1,
		xlab = "Noise sequence", ylab = "Proportion of time active",
		cex.lab=fontsizelabel, cex.axis = fontesizeaxis)
axis(side = 1, 1:2, labels = FALSE)
mtext(c("Second", "Third"), at = 1:2, line = 1.2, side = 1, cex = 1.2)
for (indiv in unique(f$unique_id)){ # indiv <- "ambient_R1_2"
	f2 <- f[f$unique_id == indiv,]
	x <- c(1,2)
	x <- x + rnorm(length(x), mean = 0, sd = 0.01) # jittering
	y <- c(f2$prop_active[f2$noise_sequence == "NS_2"] , f2$prop_active[f2$noise_sequence == "NS_3"])
	y <- y + rnorm(length(y), mean = 0, sd = 0.01) # jittering
	if (f2$condition[1] == "ambient") {
		points(x, y, pch = 19, col = col_ambient, type = "b")
	}
	else if (f2$condition[1] == "boat") {
		points(x, y, pch = 19, col = col_boat, type = "b")
	}
}
legend("topright", bty = "n", c("Ambient", "Boat"), pch = 19, col = c(col_ambient, col_boat), cex = 1.2)

###
### Main analysis: beta regression of the proportion of time active
###

rm(reg1,reg2)
reg1 <- glmmTMB(prop_active2 ~ condition*noise_sequence + (1|replicate/unique_id), data = f, family=beta_family())
summary(reg1)
reg2 <- glmmTMB(prop_active2 ~ condition+noise_sequence+ (1|replicate/unique_id), data = f, family=beta_family())
lrtest(reg1, reg2)

rm(reg1,reg2)
reg1 <- glmmTMB(prop_active2 ~ condition+noise_sequence + (1|replicate/unique_id), data = f, family=beta_family())
summary(reg1)
reg2 <- glmmTMB(prop_active2 ~ condition + (1|replicate/unique_id), data = f, family=beta_family())
lrtest(reg1, reg2)

rm(reg1,reg2)
reg1 <- glmmTMB(prop_active2 ~ condition + noise_sequence + (1|replicate/unique_id), data = f, family=beta_family())
summary(reg1)
reg2 <- glmmTMB(prop_active2 ~ noise_sequence + (1|replicate/unique_id), data = f, family=beta_family())
lrtest(reg1, reg2)

rm(reg1,reg2)
reg1 <- glmmTMB(prop_active2 ~ condition + (1|replicate/unique_id), data = f, family=beta_family())
summary(reg1)
reg2 <- glmmTMB(prop_active2 ~ 1 + (1|replicate/unique_id), data = f, family=beta_family())
lrtest(reg1, reg2)

rm(reg1,reg2)
reg1 <- glmmTMB(prop_active2 ~ noise_sequence + (1|replicate/unique_id), data = f, family=beta_family())
summary(reg1)
reg2 <- glmmTMB(prop_active2 ~ 1 + (1|replicate/unique_id), data = f, family=beta_family())
lrtest(reg1, reg2)

### Multi-model inference and AICc based model selection
rm(reg1)
reg1 <- glmmTMB(prop_active2 ~ condition*noise_sequence + (1|replicate/unique_id), data = f, family=beta_family())
ms1 <- dredge(reg1)
ms1

###
### Alternative analysis: discretization of the proportion of time active as binary data
###  - either the prey was active during the noise_sequence (discrete_activity = 1)
###  - or the prey was not active (discrete_activity = 0)
### The conclusions with this analysis are consistent with those based on beta regression
### (complementary analysis not shown in the MS)
###

f$discrete_activity[f$prop_active == 0] <- 0
f$discrete_activity[f$prop_active != 0] <- 1

table(f$discrete_activity, f$condition)

rm(reg1,reg2)
reg1 <- glmer(discrete_activity ~ condition*noise_sequence + (1|unique_id), data = f, family=binomial)
summary(reg1)
reg2 <- glmer(discrete_activity ~ condition+noise_sequence+ (1|unique_id), data = f, family=binomial)
lrtest(reg1, reg2)

rm(reg1,reg2)
reg1 <- glmer(discrete_activity ~ condition+noise_sequence + (1|unique_id), data = f, family=binomial)
summary(reg1)
reg2 <- glmer(discrete_activity ~ condition + (1|unique_id), data = f, family=binomial)
lrtest(reg1, reg2)

rm(reg1,reg2)
reg1 <- glmer(discrete_activity ~ condition + noise_sequence + (1|unique_id), data = f, family=binomial)
summary(reg1)
reg2 <- glmer(discrete_activity ~ noise_sequence + (1|unique_id), data = f, family=binomial)
lrtest(reg1, reg2)

### Multi-model inference and AICc based model selection
rm(reg1)
reg1 <- glmer(discrete_activity ~ condition*noise_sequence + (1|unique_id), data = f, family=binomial)
ms1 <- dredge(reg1)
ms1

