[![License: CC BY 4.0](https://img.shields.io/badge/License-CC_BY_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/) [![DOI:10.5281/zenodo.7706393](https://zenodo.org/badge/DOI/10.5281/zenodo.7706393.svg)](https://doi.org/10.5281/zenodo.7706393)

Datasets and R source code of the article : Fernandez-Declerck M, Rojas E, Prosnier L, Teulier L, Dechaume-Moncharmont F-X, Médoc V (2023) Adding insult to injury: anthropogenic noise intensifies predation risk by an invasive freshwater fish species. Biological Invasions http://dx.doi.org/10.1007/s10530-023-03072-w

This repository is an archive of the working versions of the project. The final files cited in the article are to be found on Zenodo (DOI: 10.5281/zenodo.7706393, https://zenodo.org/record/7706393) for long-term archiving.

	Project
	  ├── README
	  ├── data_functional_response.txt
	  ├── data_prey_behaviour.txt
	  └── R_script.R

Main dataset: 'data_functional_response.txt'. Dataset for the functional response of the predator. Fish behaviour was recorded under two noise conditions (either boat noise or ambient noise). The dataset corresponds to data frame "d" in the script. Variables description:
- id: identity of the fish
- condition: noise condition, either "boat noise" or "ambient noise"
- prey_number: number of chironomid larvae introduced in the tank
- prey_captured	: number of chironomid larvae consumed
- fish_mass	: fish body mass (g)
- swim_distance: swim distance (m)

Secondary dataset: 'data_prey_behaviour.txt'. Dataset for control experiment on prey behaviour. Prey behaviour was recorded under two noise conditions (either boat noise or ambient noise). The dataset corresponds to data frame "f" in the script. We used 20 replicates with 10 replicates for ambient noise condition, and 10 replicates for boat noise condition. Two focal prey larva were observed per replicates. Each prey larva was observed during two time periods (corresponding to two noise sequences). Variables description:
- condition: noise condition, either "boat noise" or "ambient noise"
- replicate: number of the replicate
- unique_id: identity of each focal larva
- noise_sequence: number of the noise sequence (either second or third)
- prop_inactive: proportion of time spent inactive by the focal larva
- prop_active: proportion of time spent active by the focal larva

R source code 'code R_script.R'. Complete analysis as one single R script. See comments for additional information.
